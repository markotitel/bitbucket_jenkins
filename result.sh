#!/bin/bash

JENKINS_JOB=test
REPO=test
INTERNAL_JENKINS_URL=internal
EXTERNAL_JENKINS_URL=external
USERNAME=jenkins
PASSWORD=slave
OWNER=master
BUCKET_API_V1_URL="https://bitbucket.org/api/1.0/repositories/"
BUCKET_API_V2_URL="https://bitbucket.org/api/2.0/repositories/"

if [[ -z $EXTERNAL_JENKINS_URL ]] && [[ -z $INTERNAL_JENKINS_URL ]]; then
echo 'No CI server is set.'
exit 0
fi

if [[ -z $EXTERNAL_JENKINS_URL ]]; then
EXTERNAL_JENKINS_URL=$INTERNAL_JENKINS_URL
fi

if [[ -z $INTERNAL_JENKINS_URL ]]; then
INTERNAL_JENKINS_URL=$EXTERNAL_JENKINS_URL
fi





JOB=`curl --silent http://$INTERNAL_JENKINS_URL/job/$JENKINS_JOB/lastBuild/api/json |\
python -mjson.tool |\
grep result |\
awk '{print $2}' |\
sed 's/,//g'`

URL=`curl --silent http://$INTERNAL_JENKINS_URL/job/$JENKINS_JOB/lastBuild/api/json |\
python -mjson.tool |\
grep url |\
awk '{print $2}' |\
sed 's/,//g' |\
tr -d '"' |\
sed 's/'$INTERNAL_JENKINS_URL'/'$EXTERNAL_JENKINS_URL'/g'`

echo $URL

REQUEST_ID=`curl --silent -u $USERNAME:$PASSWORD $BUCKET_API_V2_URL$OWNER/$REPO/pullrequests |\
python -mjson.tool |\
grep id |\
awk '{print $2}' |\
sed 's/,//g'`

function success()
{
  curl --silent \
  -H "Content-Type: application/json" \
  -u $USERNAME:$PASSWORD \
  -X POST -d '{"content": "'**:star:Bravo''" "''Micko,''" "''evo''" "''ti''" "'':apple:**''" "''$URL'"}' \
  "$BUCKET_API_V1_URL$OWNER/$REPO/pullrequests/${REQUEST_ID}/comments" 1>/dev/null
}

function unstable()
{
  curl --silent \
  -H "Content-Type: application/json" \
  -u $USERNAME:$PASSWORD \
  -X POST -d '{"content":  "'**Micko,''" "''dobro''" "''je''" "''ali''" "''moze''" "''bolje.**''" "''$URL'"}' \
  "$BUCKET_API_V1_URL$OWNER/$REPO/pullrequests/${REQUEST_ID}/comments" 1>/dev/null
}

function fail()
{
  curl --silent \
  -H "Content-Type: application/json" \
  -u $USERNAME:$PASSWORD \
  -X POST -d '{"content":  "'**:bow:''" "''Micko''" "''ne''" "''valja''" "''nesto.**''" "''$URL'"}' \
  "$BUCKET_API_V1_URL$OWNER/$REPO/pullrequests/${REQUEST_ID}/comments" 1>/dev/null
}

if [ "$JOB" == '"SUCCESS"' ]; then
  success
elif [ "$JOB" == '"UNSTABLE"' ]; then
  unstable
else
  fail
fi
