bitbucket_jenkins
=================

Dirty bash for posting jenkins results on bitbucket pull request comments. 

Some notes on some vars in the script.

```
JENKINS_JOB=NAME
REPO=BitBucket_repo_name
INTERNAL_JENKINS_URL=jenkins:8080
EXTERNAL_JENKINS_URL=jenkins.slave.com
USERNAME=BitBucket_username
PASSWORD=BitBucket_password
OWNER=owner_of_a_project
BUCKET_API_V1_URL="https://bitbucket.org/api/1.0/repositories/"
BUCKET_API_V2_URL="https://bitbucket.org/api/2.0/repositories/"
```
```INTERNAL_JENKINS_URL=jenkins:8080``` ```EXTERNAL_JENKINS_URL=jenkins.slave.com``` set url to jenkins server.```


```OWNER``` variable may be the same as username, but if you work in a team then this can be different.

```BUCKET_API``` vars depend on BitBucket API. So for now dont change those.
    
##Usage

Add Jenkins trigger ```Trigger builds remotely (e.g., from scripts``` and type a ```token```. Token will be used in BitBucket HOOK settings.

Put this script in workspace of Jenkins project and activate it as a```Post Build``` task.

It will get status of the latest job ```SUCCESS```, ```FAILURE``` or ```UNSTABLE``` and post it to bitbucket as a pullrequest comment.

On BitBucket side setup Pull Request POST hook, add URL to your Jenkins server, add your ```token``` and uncheck at least ```Comments``` checkbox. 
